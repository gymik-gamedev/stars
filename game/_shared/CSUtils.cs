using System;
using System.Collections.Generic;
using Godot;
using Godot.Collections;

/**
 * @brief High performance utilities
 */
public class CSUtils : Godot.Object
{
	/**
	 * @note The times are only rough estimates, but it's good enough for our
	 *       purposes.
	 * @warning The array may contain ONLY descendants of the BasicUnit class!
	 */
	public static Array<Node2D> SortUnitsByTimeToReachPoint(
		Array<Node2D> units,
		Vector2 point)
	{
		List<Tuple<Node2D, float>> unitTimePairs =
			new List<Tuple<Node2D, float>>();
		
		foreach (Node2D unit in units)
		{
			float distancePx = point.DistanceTo(unit.Position);
			float maxSpeedPxPerSec = (float) unit.Get("max_speed_px_per_sec");
			float timeToReachSec = distancePx / maxSpeedPxPerSec;
			
			unitTimePairs.Add(new Tuple<Node2D, float>(unit, timeToReachSec));
		}
		
		unitTimePairs.Sort(compareUnitFloatPairsByFloat);
		units.Clear();
		
		foreach (Tuple<Node2D, float> pair in unitTimePairs)
		{
			units.Add(pair.Item1);
		}
		
		return units;
	}
	
	public static Array<Node2D> SortUnitsByDistanceToUnit(
		Array<Node2D> units,
		Node2D targetUnit)
	{
		List<Tuple<Node2D, float>> unitDistancePairs =
			new List<Tuple<Node2D, float>>();
		
		foreach (Node2D unit in units)
		{
			float distancePx = GetDistanceBetweenEntities(unit, targetUnit);
			unitDistancePairs.Add(new Tuple<Node2D, float>(unit, distancePx));
		}
		
		unitDistancePairs.Sort(compareUnitFloatPairsByFloat);
		units.Clear();
		
		foreach (Tuple<Node2D, float> pair in unitDistancePairs)
		{
			units.Add(pair.Item1);
		}
		
		return units;
	}
	
	public static float GetDistanceToEntity(Vector2 origin, Node2D entity)
	{
		Godot.Object entityAgent = (Godot.Object) entity.Get("steering_agent");
		float entityRadius = (float) entityAgent.Get("bounding_radius");
		return origin.DistanceTo(entity.Position) - entityRadius;
	}
	
	public static float GetDistanceBetweenEntities(Node2D first, Node2D second)
	{
		Godot.Object secondAgent = (Godot.Object) second.Get("steering_agent");
		float secondRadius = (float) secondAgent.Get("bounding_radius");
		return GetDistanceToEntity(first.Position, second) - secondRadius;
	}
	
	private static int compareUnitFloatPairsByFloat(
		Tuple<Node2D, float> x,
		Tuple<Node2D, float> y)
	{
		return x.Item2.CompareTo(y.Item2);
	}
}
