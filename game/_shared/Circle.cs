using System;
using Godot;

/**
 * @brief 2D round figure with boundary made of points equidistant from a point.
 * XXX: This should be struct, but it has to be an object to be accessible from
 *      GDScript.
 */
public class Circle : Godot.Object, IEquatable<Circle>
{
	private const float CIRCLE_PRECISION = 0.001f;
	
	private float _radius;
	
	public Vector2 Position { get; set; }
	public float Radius
	{
		get { return _radius; }
		set { _radius = Mathf.Abs(value); }
	}
	
	public Circle(Vector2 position, float radius)
	{
		Position = position;
		Radius = radius;
	}
	
	public Circle(Circle another)
	{
		Position = another.Position;
		Radius = another.Radius;
	}
	
	public static bool operator == (Circle circle1, Circle circle2)
	{
		if ((object) circle1 == null || (object) circle2 == null)
		{
			return object.Equals(circle1, circle2);
		}
		
		return circle1.Equals(circle2);
	}
	
	public static bool operator != (Circle circle1, Circle circle2)
	{
		return !(circle1 == circle2);
	}
	
	public bool Intersects(Circle circle)
	{
		float distance = Position.DistanceTo(circle.Position);
		float requiredDistance = Radius + circle.Radius;
		return reducePrecision(distance) < reducePrecision(requiredDistance);
	}
	
	public float DistanceTo(Circle circle)
	{
		return Position.DistanceTo(circle.Position) - Radius - circle.Radius;
	}
	
	public bool Equals(Circle circle)
	{
		return reducePrecision(Radius) == reducePrecision(circle.Radius)
			&& reducePrecision(Position) == reducePrecision(circle.Position);
	}
	
	public override bool Equals(object obj)
	{
		Circle castObj = obj as Circle;
		
		if (castObj == null)
		{
			return false;
		}
		
		return Equals(castObj);
	}
	
	public override int GetHashCode()
	{
		return Tuple.Create(reducePrecision(this)).GetHashCode();
	}
	
	public override string ToString()
	{
		return String.Format("({0}, {1})", Position, Radius);
	}
	
	private static float reducePrecision(float num)
	{
		return Mathf.Stepify(num, CIRCLE_PRECISION);
	}
	
	private static Vector2 reducePrecision(Vector2 vec)
	{
		return new Vector2(reducePrecision(vec.x), reducePrecision(vec.y));
	}
	
	private static Circle reducePrecision(Circle circle)
	{
		return new Circle(
			reducePrecision(circle.Position),
			reducePrecision(circle.Radius)
		);
	}
}
