extends Node
# Singleton that allows easy access to C# classes (excl. C# singletons)

var Circle: CSharpScript = Utils.get_shared_resource_by_name("Circle.cs")
var CSUtils: CSharpScript = Utils.get_shared_resource_by_name("CSUtils.cs")
