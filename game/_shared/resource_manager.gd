class_name ResourceManager
extends Node
# Object that takes care of materials and parts management

signal item_in_stock(item)
signal item_out_of_stock(item)
signal materials_count_changed(count)
signal parts_count_changed(count)

enum Item { BASIC_UNIT, DASH, TANK, MINER, SHIELD_ONE_SEC, WARP_DRIVE_REPAIR }
enum Reward { ENEMY_UNIT_DESTROYED, ASTEROID_MINED, SPACE_JUNK_MINED }

const ITEM_PRICES: Dictionary = {
	"BASIC_UNIT": { "materials": 10, "parts": 0 },
	"DASH": { "materials": 5, "parts": 0 },
	"TANK": { "materials": 20, "parts": 0 },
	"MINER": { "materials": 20, "parts": 0 },
	"SHIELD_ONE_SEC": { "materials": 10, "parts": 0 },
	"WARP_DRIVE_REPAIR": { "materials": 0, "parts": 25 }
}
const REWARD_RANGES: Dictionary = {
	"ENEMY_UNIT_DESTROYED": {
		"materials": { "min": 0, "max": 20 },
		"parts": { "min": 0, "max": 0 }
	},
	"ASTEROID_MINED": {
		"materials": { "min": 100, "max": 500 },
		"parts": { "min": 0, "max": 0 }
	},
	"SPACE_JUNK_MINED": {
		"materials": { "min": 0, "max": 0 },
		"parts": { "min": 0, "max": 2 }
	}
}

var materials: int = 70 setget _set_materials
var parts: int = 0 setget _set_parts

func buy_item(item: int) -> bool:
	var item_str: String = _item_to_string(item)
	
	if item_str == "":
		return false
	
	if not _is_item_in_stock(item):
		return false
	
	self.materials -= ITEM_PRICES[item_str].materials
	self.parts -= ITEM_PRICES[item_str].parts
	return true


func redeem_reward(reward: int) -> void:
	var reward_str: String = _reward_to_string(reward)
	
	if reward_str == "":
		return
	
	var ranges: Dictionary = REWARD_RANGES[reward_str]
	self.materials += Utils.randi_range( \
			ranges.materials.min, ranges.materials.max)
	self.parts += Utils.randi_range(ranges.parts.min, ranges.parts.max)


func _item_to_string(item: int) -> String:
	var item_as_string: String = Utils.get_dict_key_from_value(Item, item)
	
	if item_as_string == "":
		push_error("Invalid item: " + item as String + ".")
	
	return item_as_string


func _reward_to_string(reward: int) -> String:
	var reward_as_string: String = Utils.get_dict_key_from_value(Reward, reward)
	
	if reward_as_string == "":
		push_error("Invalid reward: " + reward as String + ".")
	
	return reward_as_string


func _string_to_item(item_name: String) -> int:
	if not Item.has(item_name):
		push_error("Invalid item name: " + item_name + ".")
		return -1
	
	return Item[item_name]


func _is_item_in_stock(item: int) -> bool:
	return _will_item_be_in_stock(item, materials, parts)


func _will_item_be_in_stock(item: int, materials: int, parts: int) -> bool:
	var price: Dictionary = ITEM_PRICES[_item_to_string(item)]
	return materials >= price.materials and parts >= price.parts


func _set_materials(new_count: int) -> void:
	_update_stock(new_count - materials, 0)


func _set_parts(new_count: int) -> void:
	_update_stock(0, new_count - parts)


func _update_stock(materials_change: int, parts_change: int) -> void:
	if materials_change == 0 and parts_change == 0:
		return
	
	var new_materials: int = materials + materials_change
	var new_parts: int = parts + parts_change
	
	for item_str in ITEM_PRICES:
		var item: int = _string_to_item(item_str)
		
		if not _is_item_in_stock(item) \
				and _will_item_be_in_stock(item, new_materials, new_parts):
			emit_signal("item_in_stock", item)
		
		if _is_item_in_stock(item) \
				and not _will_item_be_in_stock(item, new_materials, new_parts):
			emit_signal("item_out_of_stock", item)
	
	if materials_change != 0:
		materials = new_materials
		emit_signal("materials_count_changed", materials)
	
	if parts_change != 0:
		parts = new_parts
		emit_signal("parts_count_changed", parts)
