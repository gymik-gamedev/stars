class_name Utils
extends Object
# Various general purpose utilities

const RECT2_EMPTY: Rect2 = Rect2(Vector2.ZERO, Vector2.ZERO)
const BASE_PATH: String = "res://game"
const SHARED_DIR_NAME: String = "_shared"

static func rand_choice(options: Array):
	return options[randi() % options.size()]


static func pascal_case_to_snake_case(string: String) -> String:
	var i: int = 0
	
	while i < string.length():
		if string.length() - i > 1 and is_lower_case(string[i]) \
				and (is_upper_case(string[i + 1]) or is_digit(string[i + 1])) \
				or string.length() - i > 2 and is_upper_case(string[i]) \
				and (is_digit(string[i + 1]) or is_upper_case(string[i + 1])) \
				and is_lower_case(string[i + 2]):
			string = string.insert(i + 1, "_")
		
		string[i] = string[i].to_lower()
		i += 1
	
	return string


static func is_upper_case(chars: String) -> bool:
	# XXX: Works only for ASCII!
	return is_char_in_range("A", "Z", chars)


static func is_lower_case(chars: String) -> bool:
	# XXX: Works only for ASCII!
	return is_char_in_range("a", "z", chars)


static func is_digit(digits: String) -> bool:
	# XXX: Returns true only if characters are digits -- returns false if the
	#      string contains decimal point.
	return is_char_in_range("0", "9", digits)


static func is_char_in_range(range_min: String, range_max: String, \
		chars: String) -> bool:
	# Look up ASCII table to understand what this does
	if range_min.length() > 1 or range_max.length() > 1:
		push_error("Bad value: range_min and range_max must be characters.")
		return false
	
	var char_code_min: int = ord(range_min)
	var char_code_max: int = ord(range_max)
	
	for character in chars:
		var char_code: int = ord(character)
		
		if char_code < char_code_min or char_code > char_code_max:
			return false
	
	return true


static func flip_rotation_direction(rotation_rad: float) -> float:
	return -sign(rotation_rad) * 2 * PI + normalize_rotation(rotation_rad)


static func normalize_rotation(rotation_rad: float) -> float:
	if rotation_rad >= 0:
		return wrapf(rotation_rad, 0.0, 2 * PI)
	else:
		return wrapf(rotation_rad, -2 * PI, 0.0)


static func unify_rotation(rotation_rad: float) -> float:
	if rotation_rad >= 0:
		return rotation_rad
	else:
		return flip_rotation_direction(rotation_rad)


static func add_rotation(addend1: float, addend2: float) -> float:
	return wrapf(addend1 + addend2, 0.0, 2 * PI)


static func subtract_rotation(minuend: float, subtrahend: float) -> float:
	return wrapf(minuend - subtrahend, 0.0, 2 * PI)


static func is_point_on_line_segment(boundary1: Vector2, boundary2: Vector2, \
		point: Vector2) -> bool:
	var boundary1_to_boundary2: float = boundary1.angle_to_point(boundary2)
	var boundary1_to_point: float = boundary1.angle_to_point(point)
	
	var boundary2_to_boundary1: float = boundary2.angle_to_point(boundary1)
	var boundary2_to_point: float = boundary2.angle_to_point(point)
	
	return is_equal_approx(boundary1_to_boundary2, boundary1_to_point) \
			and is_equal_approx(boundary2_to_boundary1, boundary2_to_point)


static func rand_point_on_viewport_edge(viewport_size: Vector2, \
		linear_offset_px: float = 0.0) -> Vector2:
	var random_point: Vector2 = Vector2.ZERO
	var edges: Array = [ Vector2.UP, Vector2.DOWN, Vector2.LEFT, Vector2.RIGHT ]
	
	match rand_choice(edges):
		Vector2.UP:
			random_point.x = rand_range(0.0, viewport_size.x)
			random_point.y = 0.0 - linear_offset_px
		Vector2.DOWN:
			random_point.x = rand_range(0.0, viewport_size.x)
			random_point.y = viewport_size.y + linear_offset_px
		Vector2.LEFT:
			random_point.x = 0.0 - linear_offset_px
			random_point.y = rand_range(0.0, viewport_size.y)
		Vector2.RIGHT:
			random_point.x = viewport_size.x + linear_offset_px
			random_point.y = rand_range(0.0, viewport_size.y)
	
	return random_point


static func calculate_braking_distance(current_speed_px: float, \
		max_speed_px: float) -> float:
	var speed_to_max_ratio: float = current_speed_px / max_speed_px
	var braking_distance_to_speed_ratio: float = speed_to_max_ratio * 0.5
	
	return current_speed_px * braking_distance_to_speed_ratio


static func rand_chance(chance: float) -> bool:
	return randi() % pow(chance, -1) as int == 0


static func randi_range(min_val: int, max_val: int) -> int:
	return randi() % (max_val - min_val + 1) + min_val


static func get_dict_key_from_value(dict: Dictionary, value) -> String:
	for key in dict.keys():
		if dict[key] == value:
			return key
	
	return ""


static func get_scene_by_name(name: String, base_path: String) -> PackedScene:
	var name_converted: String = pascal_case_to_snake_case(name)
	var dir_path: String = base_path + "/" + name_converted
	var scene_path: String = dir_path + "/" + name_converted + ".tscn"
	var script_path: String = dir_path + "/scripts/" + name_converted + ".gd"
	
	if ResourceLoader.exists(scene_path, "PackedScene"):
		return ResourceLoader.load(scene_path) as PackedScene
	elif ResourceLoader.exists(script_path, "Script"):
		var node = ResourceLoader.load(script_path).new()
		
		var packed_scene: PackedScene = PackedScene.new()
		packed_scene.pack(node)
		
		return packed_scene
	else:
		return null


static func get_resource_by_name(filename: String, \
		base_path: String) -> Resource:
	var path: String = base_path + "/" + filename
	
	if ResourceLoader.exists(path):
		return ResourceLoader.load(path)
	else:
		push_error("Resource not found: " + path + ".")
		return null


static func get_shared_resource_by_name(filename: String) -> Resource:
	return get_resource_by_name(filename, BASE_PATH + "/" + SHARED_DIR_NAME)
