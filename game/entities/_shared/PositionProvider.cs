using System;
using System.Collections.Generic;
using Godot;

/**
 * @brief Provides unoccupied positions matching given criteria to units.
 */
public class PositionProvider : Node
{
	private List<Circle> _occupiedSpots = new List<Circle>();
	
	public Vector2 RequestPositionNearPoint(Circle self, Vector2 point)
	{
		self.Position = point;
		Circle result = findUnoccupiedSpot(self);
		
		if (result == null)
		{
			GD.PushError("PositionProvider: No spot found!");
			return Vector2.Zero;
		}
		
		return result.Position;
	}
	
	public Vector2 RequestFiringPosition(
		Circle self,
		Circle target,
		float fireRange)
	{
		if (self.DistanceTo(target) > fireRange)
		{
			Vector2 dirToSelf = target.Position.DirectionTo(self.Position);
			self.Position = target.Position + dirToSelf * fireRange;
		}
		
		Predicate<Circle> criteria =
			x => x.DistanceTo(target) <= fireRange;
		
		Circle result = findUnoccupiedSpot(self, criteria);
		
		if (result == null)
		{
			GD.PushError("PositionProvider: No spot found!");
			return Vector2.Zero;
		}
		
		return result.Position;
	}
	
	public Vector2 RequestNearestStopPosition(
		Circle self,
		Vector2 direction,
		float brakingDistance)
	{
		Predicate<Circle> criteria =
			x => self.DistanceTo(x) >= brakingDistance
				&& self.Position.DirectionTo(x.Position).Dot(direction) > 0.0F;
		
		Circle start = new Circle(self);
		start.Position += direction * brakingDistance;
		
		Circle result = findUnoccupiedSpot(self, criteria);
		
		if (result == null)
		{
			GD.PushError("PositionProvider: No spot found!");
			return Vector2.Zero;
		}
		
		return result.Position;
	}
	
	public void FreePosition(Circle self)
	{
		_occupiedSpots.Remove(self);
	}
	
	private Circle findUnoccupiedSpot(
		Circle start,
		Predicate<Circle> growthCriteria = null)
	{
		if (growthCriteria == null)
		{
			growthCriteria = delegate { return true; };
		}
		
		List<Circle> spots = new List<Circle>();
		spots.Add(start);
		
		for (int i = 0; i < spots.Count; i++)
		{
			Circle collider = getSpotCollider(spots[i]);
			
			if (collider == null)
			{
				_occupiedSpots.Add(spots[i]);
				return spots[i];
			}
			
			List<Circle> neighbors = getSurroundingCircles(
				collider,
				start.Radius
			);
			
			neighbors.RemoveAll(x => !growthCriteria(x));
			neighbors.RemoveAll(x => spots.Contains(x));
			spots.AddRange(neighbors);
		}
		
		// No position matching given criteria found
		return null;
	}
	
	private Circle getSpotCollider(Circle spot)
	{
		return _occupiedSpots.Find(x => x.Intersects(spot));
	}
	
	private static List<Circle> getSurroundingCircles(
		Circle origin,
		float neighborRadius)
	{
		float distanceBetweenCenters = origin.Radius + neighborRadius;
		float cosX = neighborRadius / distanceBetweenCenters;
		float polygonAngleRad = 2 * Mathf.Acos(cosX);
		int circlesCount = getCirclesCountFromPolygonAngle(polygonAngleRad);
		
		float rotStep = 2 * Mathf.Pi / circlesCount;
		List<Circle> neighbors = new List<Circle>();
		Vector2 toNeighbor = Vector2.Up * distanceBetweenCenters;
		
		for (int i = 0; i < circlesCount; i++)
		{
			toNeighbor = toNeighbor.Rotated(rotStep);
			neighbors.Add(
				new Circle(origin.Position + toNeighbor, neighborRadius)
			);
		}
		
		return neighbors;
	}
	
	private static int getCirclesCountFromPolygonAngle(float angleRad)
	{
		int sidesCount = 3;
		float angleStep = Mathf.Pi;
		float polygonAngleSum = angleStep;
		
		while (angleRad >= polygonAngleSum / sidesCount) {
			sidesCount++;
			polygonAngleSum += angleStep;
		}
		
		return sidesCount - 1;
	}
}
