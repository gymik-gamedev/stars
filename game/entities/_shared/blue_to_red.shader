shader_type canvas_item;

void fragment() {
	vec4 tex_clr = texture(TEXTURE, UV);
	
	float tex_b = tex_clr.b;
	tex_clr.b = tex_clr.r;
	tex_clr.r = tex_b;
	
	COLOR = tex_clr;
}