class_name EntityUtils
extends Object
# Various utilities for working with entities

const ENTITIES_PATH: String = Utils.BASE_PATH + "/entities"
const HEALTHBAR_RELATIVE_Z_INDEX: int = 2

static func get_entity_scene_by_name(entity_name: String) -> PackedScene:
	var scene: PackedScene = Utils.get_scene_by_name(entity_name, ENTITIES_PATH)
	
	if scene == null:
		push_error("Entity not found: " + entity_name + ".")
	
	return scene


static func get_shared_resource_by_name(resource_filename: String) -> Resource:
	return Utils.get_resource_by_name(resource_filename, \
			ENTITIES_PATH + "/" + Utils.SHARED_DIR_NAME)


static func attach_healthbar_to_object(object: SpaceObject, bar_size: Vector2, \
		margin_px: float) -> void:
	# XXX: Do not run before object's _ready!
	# No explicit type to avoid cyclic reference
	var bar := UIUtils.get_ui_scene_by_name("Healthbar").instance()
	var radius: float = object.steering_agent.bounding_radius
	var node: Node2D = Node2D.new()
	
	object.add_child(node)
	node.z_index = HEALTHBAR_RELATIVE_Z_INDEX
	node.add_child(bar)
	
	bar.rect_size = bar_size
	bar.rect_scale.x = pow(object.scale.x, -1)
	bar.rect_scale.y = pow(object.scale.y, -1)
	bar.rect_rotation = -rad2deg(object.rotation)
	bar.rect_global_position = object.position
	bar.rect_global_position.x -= bar_size.x / 2
	bar.rect_global_position.y -= bar_size.y + radius + margin_px
	
	bar.set_tracked_object(object, true)


static func get_circle_from_entity(entity: Node2D) -> Object:
	# XXX: Entity must be Mothership or SpaceObject. I couldn't place proper
	#      checks here as it could trigger cyclic references.
	return CSharp.Circle.new(entity.position, \
			entity.steering_agent.bounding_radius)


static func is_entity_facing_entity(origin: Node2D, entity: Node2D) -> bool:
	# XXX: Both entities must be Mothership or SpaceObject.
	var origin_rot: float = Utils.unify_rotation(origin.rotation)
	var entity_relative_pos: Vector2 = entity.position - origin.position
	var rot_to_entity: float = Utils.unify_rotation( \
			Vector2.UP.angle_to(entity_relative_pos))
	
	var relative_bound_up: Vector2 = entity_relative_pos \
			+ Vector2.UP.rotated(rot_to_entity + PI / 2) \
			* entity.steering_agent.bounding_radius
	var relative_bound_down: Vector2 = entity_relative_pos \
			+ Vector2.UP.rotated(rot_to_entity - PI / 2) \
			* entity.steering_agent.bounding_radius
	
	var rot_to_bound_up: float = Utils.unify_rotation( \
			Vector2.UP.angle_to(relative_bound_up))
	var rot_to_bound_down: float = Utils.unify_rotation( \
			Vector2.UP.angle_to(relative_bound_down))
	
	return origin_rot > rot_to_bound_down and origin_rot < rot_to_bound_up


static func get_distance_to_entity(origin: Vector2, entity: Node2D) -> float:
	# XXX: Entity must be Mothership or SpaceObject.
	return origin.distance_to(entity.position) \
			- entity.steering_agent.bounding_radius


static func get_distance_between_entities(first: Node2D, \
		second: Node2D) -> float:
	# XXX: Both entities must be Mothership or SpaceObject.
	return get_distance_to_entity(first.position, second) \
			- first.steering_agent.bounding_radius


static func get_silhouette_from_sprite(sprite: Sprite) -> Sprite:
	var silhouette: Sprite = sprite.duplicate()
	
	var material: ShaderMaterial = ShaderMaterial.new()
	material.shader = get_shared_resource_by_name("silhouette.shader")
	
	silhouette.material = material
	return silhouette


static func get_glow_from_sprite(sprite: Sprite, width: float, \
		color: Color) -> Node2D:
	var silhouette: Sprite = get_silhouette_from_sprite(sprite)
	var glow: Node2D = Node2D.new()
	glow.name = "Glow"
	
	var dirs: Array = [
		Vector2(0.0, -1.0),
		Vector2(1.0, -1.0),
		Vector2(1.0, 0.0),
		Vector2(1.0, 1.0),
		Vector2(0.0, 1.0),
		Vector2(-1.0, 1.0),
		Vector2(-1.0, 0.0),
		Vector2(-1.0, -1.0)
	]

	for dir in dirs:
		var glow_part: Sprite = silhouette.duplicate()
		glow_part.position.x += dir.x * width
		glow_part.position.y += dir.y * width
		glow_part.modulate = color
		glow.add_child(glow_part)
	
	return glow
