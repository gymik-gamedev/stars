shader_type canvas_item;
render_mode unshaded;

void fragment() {
	vec4 tex_clr = texture(TEXTURE, UV);
	
	if (tex_clr.a == 0.0) {
		return;
	}
	
	tex_clr.rgb = vec3(1.0, 1.0, 1.0);
	COLOR = tex_clr;
}
