class_name Asteroid
extends SpaceObject
# Harvestable autonomous space object

const MOTHERSHIP_POSITION: Vector2 = Vector2(960.0, 540.0)
const MOTHERSHIP_RADIUS_PX: float = 210.0
const MIN_SCALE: float = 0.3
const MAX_SCALE: float = 1.0
const PATH_MIN_LENGTH_PX: float = 1000.0
const PATH_MIN_DISTANCE_FROM_VIEWPORT_PX: float = 100.0
const PATH_BOUNDARY_OFFSET_FROM_VIEWPORT_PX: float = 300.0

var BASE_RESOLUTION: Vector2 = Vector2( \
		ProjectSettings.get_setting("display/window/size/width"), \
		ProjectSettings.get_setting("display/window/size/height"))
var VIEWPORT_BOUNDARY: Rect2 = Rect2(Vector2.ZERO, BASE_RESOLUTION)

var speed_px_per_sec: float = 15.0
var rotation_rad_per_sec: float = 0.2

var _direction: Vector2 = Vector2.ZERO

onready var _collision_shape: CollisionShape2D = get_node("CollisionShape2D")

func _init() -> void:
	max_hp = 250
	hp = max_hp


func _ready() -> void:
	# XXX: Since this _ready always propagates to SpaceJunk as well, make sure
	#      that SpaceJunk's collision shape has radius > height.
	var shape: Shape2D = _collision_shape.shape
	
	_set_random_scale()
	steering_agent.bounding_radius = shape.radius * max(scale.x, scale.y)
	max_hp *= max(scale.x, scale.y)
	hp = max_hp
	
	var path_start: Vector2 = Utils.rand_point_on_viewport_edge(BASE_RESOLUTION)
	var path_end: Vector2 = Utils.rand_point_on_viewport_edge(BASE_RESOLUTION)
	
	while not _is_path_possible(path_start, path_end):
		path_end = Utils.rand_point_on_viewport_edge(BASE_RESOLUTION)
	
	_direction = path_start.direction_to(path_end)
	
	# offset start pos away from viewport
	position = _direction.rotated(PI) * PATH_BOUNDARY_OFFSET_FROM_VIEWPORT_PX \
			+ path_start


func _physics_process(delta: float) -> void:
	var bounds: Rect2 = \
			VIEWPORT_BOUNDARY.grow(PATH_BOUNDARY_OFFSET_FROM_VIEWPORT_PX)
	
	if not _is_point_in_bounds(position, bounds):
		queue_free()
	
	move_and_slide(_direction * speed_px_per_sec)
	rotation += rotation_rad_per_sec * delta


func _is_path_possible(path_start: Vector2, path_end: Vector2) -> bool:
	var path_center: Vector2 = (path_start + path_end) / 2
	
	if path_start.distance_to(path_end) < PATH_MIN_LENGTH_PX:
		return false
	
	# checks if path isn't too close to viewport edge
	var bounds: Rect2 = \
			VIEWPORT_BOUNDARY.grow(-PATH_MIN_DISTANCE_FROM_VIEWPORT_PX)
	
	if not _is_point_in_bounds(path_center, bounds):
		return false
	
	# checks if path isn't too close to mothership
	var angle_to_mothership: float = \
			path_start.angle_to_point(MOTHERSHIP_POSITION)
	
	var line_segment_start: Vector2 = \
			Vector2.RIGHT.rotated(angle_to_mothership).rotated(PI / 2)
	var line_segment_end: Vector2 = line_segment_start.rotated(-PI)
	
	line_segment_start *= MOTHERSHIP_RADIUS_PX + steering_agent.bounding_radius
	line_segment_start += MOTHERSHIP_POSITION
	
	line_segment_end *= MOTHERSHIP_RADIUS_PX + steering_agent.bounding_radius
	line_segment_end += MOTHERSHIP_POSITION
	
	var intersection_point: Vector2 = Geometry.line_intersects_line_2d(
		line_segment_start, line_segment_start.direction_to(line_segment_end),
		path_start, path_start.direction_to(path_end)
	)
	
	if Utils.is_point_on_line_segment(line_segment_start, line_segment_end, \
			intersection_point):
		return false
	
	return true


func _is_point_in_bounds(point: Vector2, bounds: Rect2) -> bool:
	return bounds.has_point(point)


func _set_random_scale() -> void:
	scale.x = rand_range(MIN_SCALE, MAX_SCALE)
	scale.y = scale.x
