class_name BasicUnit
extends Unit
# Basic unit type

enum State {
	IDLE,
	MOVING_TO_POS,
	MOVING_TO_TARGET,
	STOPPING_NEAR_TARGET,
	TURNING_TO_TARGET,
	FIRING
}

const ARRIVE_PRIORITY: float = 1.0
const ARRIVE_TOLERANCE_PX: float = 0.0
const ARRIVE_PRECISION_PX: float = 5.0
const LOOK_WHERE_YOU_GO_PRIORITY: float = 1.0
const LOOK_WHERE_YOU_GO_TOLERANCE_RAD: float = 0.01
const FACE_PRIORITY: float = 1.0
const FACE_TOLERANCE_RAD: float = 0.01
const AVOID_PRIORITY: float = 5.0
const AVOIDANCE_AREA_SIZE_MULTIPLIER: float = 1.5
const SEEK_PRIORITY: float = 1.0
const LINEAR_DRAG_PERCENT: float = 0.02
const ANGULAR_DRAG_PERCENT: float = 0.02
const STATIONARY_RELATIVE_Z_INDEX: int = 0
const MOVING_RELATIVE_Z_INDEX: int = 1
const SELECTED_GLOW_RELATIVE_Z_INDEX: int = -1
const IDLE_GLOW_RELATIVE_Z_INDEX: int = -2
const SELECTED_GLOW_COLOR: Color = Color.limegreen
const IDLE_GLOW_COLOR: Color = Color.deeppink
const GLOW_WIDTH_PX: float = 2.0
const AVOIDANCE_SPEED_STEP: float = 20.0

var max_speed_px_per_sec: float = 200.0 setget _set_max_speed
var max_angular_speed_rad_per_sec: float = 0.5 * PI \
		setget _set_max_angular_speed
var firerate_bullet_per_sec: float = 2.0
var fire_range_px: float = 150.0
var side: int = Unit.Side.INVALID setget set_side

var _state: int = State.IDLE
var _target: WeakRef = weakref(null) setget _set_target
var _target_queue: Array = []
var _time_elapsed_since_bullet_fired: float = 0.0
var _last_avoidance_size: float = 0.0
var _target_accel: GSAITargetAcceleration = GSAITargetAcceleration.new()
var _arrive_target: GSAIAgentLocation = GSAIAgentLocation.new()
var _behavior_arrive: GSAIArrive = null
var _behavior_look: GSAILookWhereYouGo = null
var _face_target: GSAIAgentLocation = GSAIAgentLocation.new()
var _behavior_face: GSAIFace = null
var _avoid_context: GSAIInfiniteProximity = null
var _behavior_avoid: GSAIAvoidCollisions = null
var _seek_target: GSAIAgentLocation = GSAIAgentLocation.new()
var _behavior_seek: GSAISeek = null
var _behaviors_combined: GSAIBlend = null

var _bullet_scene: PackedScene = EntityUtils.get_entity_scene_by_name("Bullet")
var _blue_to_red: Shader = \
		EntityUtils.get_shared_resource_by_name("blue_to_red.shader")

onready var _firing_positions: Line2D = get_node("FiringPositions")
onready var _sprite: Sprite = get_node("Sprite")
onready var _collision_shape: CollisionShape2D = get_node("CollisionShape2D")
onready var _avoidance_area: Area2D = get_node("CollisionPredictionArea")
onready var _avoidance_radius: CollisionShape2D = \
		_avoidance_area.get_node("Radius")
onready var _visibility_area: Area2D = get_node("VisibilityArea")
onready var _selected_glow: Node2D = EntityUtils.get_glow_from_sprite( \
		_sprite, GLOW_WIDTH_PX, SELECTED_GLOW_COLOR)
onready var _idle_glow: Node2D = EntityUtils.get_glow_from_sprite( \
		_sprite, GLOW_WIDTH_PX, IDLE_GLOW_COLOR)

static func get_unit_by_name(unit_type: String) -> Unit:
	var unit_scene: PackedScene = \
			EntityUtils.get_entity_scene_by_name(unit_type)
	
	if unit_scene == null or not unit_scene.can_instance():
		push_error("Error: cannot instance " + unit_type + " scene.")
		return null
	
	var unit = unit_scene.instance()
	
	if not unit is Unit:
		unit.free()
		push_error("Bad type: entity " + unit_type + " is not a unit.")
		return null
	
	return unit


func _init() -> void:
	# Variables in child classes are initialized before variables in parent
	# classes for some weird reason, so we gotta initialize those that depend
	# on parent class' variables here instead.
	_behavior_arrive = GSAIArrive.new(steering_agent, _arrive_target)
	_behavior_look = GSAILookWhereYouGo.new(steering_agent)
	_behavior_face = GSAIFace.new(steering_agent, _face_target)
	_avoid_context = GSAIInfiniteProximity.new(steering_agent, [])
	_behavior_avoid = GSAIAvoidCollisions.new(steering_agent, _avoid_context)
	_behavior_seek = GSAISeek.new(steering_agent, _seek_target)
	_behaviors_combined = GSAIBlend.new(steering_agent)
	
	steering_agent.linear_speed_max = max_speed_px_per_sec
	steering_agent.linear_acceleration_max = max_speed_px_per_sec
	steering_agent.angular_speed_max = max_angular_speed_rad_per_sec
	steering_agent.angular_acceleration_max = max_angular_speed_rad_per_sec
	steering_agent.linear_drag_percentage = LINEAR_DRAG_PERCENT
	steering_agent.angular_drag_percentage = ANGULAR_DRAG_PERCENT
	
	_behavior_arrive.arrival_tolerance = ARRIVE_TOLERANCE_PX
	_behavior_arrive.deceleration_radius = max_speed_px_per_sec
	
	_behavior_look.alignment_tolerance = LOOK_WHERE_YOU_GO_TOLERANCE_RAD
	_behavior_look.deceleration_radius = max_angular_speed_rad_per_sec / 5
	
	_behavior_face.alignment_tolerance = FACE_TOLERANCE_RAD
	_behavior_face.deceleration_radius = max_angular_speed_rad_per_sec / 5
	
	_behaviors_combined.add(_behavior_arrive, ARRIVE_PRIORITY)
	_behaviors_combined.add(_behavior_look, LOOK_WHERE_YOU_GO_PRIORITY)
	_behaviors_combined.add(_behavior_face, FACE_PRIORITY)
	_behaviors_combined.add(_behavior_avoid, AVOID_PRIORITY)
	_behaviors_combined.add(_behavior_seek, SEEK_PRIORITY)
	
	max_hp = 50
	hp = max_hp


func _ready() -> void:
	var shape: CircleShape2D = _collision_shape.shape
	steering_agent.bounding_radius = shape.radius * max(scale.x, scale.y)
	
	_avoidance_area.connect("body_entered", self, "_possible_collider_sighted")
	_avoidance_area.connect("body_exited", self, \
			"_possible_collider_out_of_sight")
	
	_visibility_area.connect("body_entered", self, "_object_sighted")
	
	add_child(_selected_glow)
	_selected_glow.visible = false
	_selected_glow.z_index = SELECTED_GLOW_RELATIVE_Z_INDEX
	
	add_child(_idle_glow)
	_idle_glow.visible = false
	_idle_glow.z_index = IDLE_GLOW_RELATIVE_Z_INDEX
	
	_switch_state(_state)
	move_to_pos(position)
	set_side(Unit.Side.FRIENDS)


func _physics_process(delta: float) -> void:
	_process_state(delta)
	
	_behaviors_combined.calculate_steering(_target_accel)
	steering_agent._apply_steering(_target_accel, delta)


func attack(target: Node2D) -> void:
	if target is Mothership or target is SpaceObject:
		var old_target: Node2D = _target.get_ref()
		if old_target != null:
			old_target.disconnect("hp_changed", self, "_check_target_hp_change")
		
		self._target = weakref(target)
		target.connect("hp_changed", self, "_check_target_hp_change")
		_free_last_arrive_target()
		_switch_state(State.MOVING_TO_TARGET)
	else:
		push_error("Bad value: target must be valid Mothership or SpaceObject.")


func move_to_pos(target_pos: Vector2) -> void:
	_free_last_arrive_target()
	
	var current_spot: Object = EntityUtils.get_circle_from_entity(self)
	target_pos = PositionProvider.RequestPositionNearPoint( \
			current_spot, target_pos)
	
	_arrive_target.position = GSAIUtils.to_vector3(target_pos)
	_switch_state(State.MOVING_TO_POS)


func enable_glow(glow_type: int) -> void:
	# XXX: Do not run this before _ready!
	match glow_type:
		GlowType.SELECTED:
			_selected_glow.visible = true
		GlowType.IDLE:
			_idle_glow.visible = true
		_:
			push_error("Bad type: glow must be member of GlowType enum.")


func disable_glow(glow_type: int) -> void:
	# XXX: Do not run this before _ready,
	match glow_type:
		GlowType.SELECTED:
			_selected_glow.visible = false
		GlowType.IDLE:
			_idle_glow.visible = false
		_:
			push_error("Bad type: glow must be member of GlowType enum.")


func set_side(new_side: int) -> void:
	# XXX: Do not run this before _ready, it depends on onready variables!
	if new_side == side:
		return
	
	if new_side == Unit.Side.FRIENDS:
		collision_layer = 0b00001
		collision_mask = 0b11010
		_sprite.material = null
	elif new_side == Unit.Side.ENEMIES:
		var material: ShaderMaterial = ShaderMaterial.new()
		material.shader = _blue_to_red
		
		collision_layer = 0b00010
		collision_mask = 0b11001
		_sprite.material = material
	else:
		push_error("Bad type: side must be member of EntityUtils.Side enum.")
		return
	
	side = new_side


func _fire_bullet(delta: float) -> void:
	# TODO: Adjust firing positions based on the size of the fired object?
	if _time_elapsed_since_bullet_fired < 1 / firerate_bullet_per_sec:
		_time_elapsed_since_bullet_fired += delta
		return
	
	var firing_direction: Vector2 = Vector2.UP.rotated(rotation)
	
	for i in _firing_positions.get_point_count():
		var bullet: Bullet = _bullet_scene.instance()
		var firing_position: Vector2 = \
				_firing_positions.get_point_position(i).rotated(rotation)
		
		bullet.position = position + firing_position
		bullet.set_direction(firing_direction)
		bullet.set_range(fire_range_px)
		
		get_parent().add_child(bullet)
		bullet.set_side(side)
	
	_time_elapsed_since_bullet_fired = 0.0


func _switch_state(new_state: int) -> void:
	# XXX: Do not run before _ready!
	match new_state:
		State.IDLE:
			_behavior_arrive.is_enabled = false
			_behavior_avoid.is_enabled = false
			_behavior_face.is_enabled = false
			_behavior_look.is_enabled = false
			_behavior_seek.is_enabled = false
			
			z_index = STATIONARY_RELATIVE_Z_INDEX
			if side == Side.FRIENDS:
				enable_glow(GlowType.IDLE)
		State.MOVING_TO_POS:
			_behavior_arrive.is_enabled = true
			_behavior_avoid.is_enabled = true
			_behavior_face.is_enabled = false
			_behavior_look.is_enabled = true
			_behavior_seek.is_enabled = false
			
			z_index = MOVING_RELATIVE_Z_INDEX
			disable_glow(GlowType.IDLE)
		State.MOVING_TO_TARGET:
			_behavior_arrive.is_enabled = false
			_behavior_avoid.is_enabled = true
			_behavior_face.is_enabled = false
			_behavior_look.is_enabled = true
			_behavior_seek.is_enabled = true
			
			z_index = MOVING_RELATIVE_Z_INDEX
			disable_glow(GlowType.IDLE)
		State.STOPPING_NEAR_TARGET:
			_behavior_arrive.is_enabled = true
			_behavior_avoid.is_enabled = true
			_behavior_face.is_enabled = false
			_behavior_look.is_enabled = true
			_behavior_seek.is_enabled = false
			
			z_index = MOVING_RELATIVE_Z_INDEX
			disable_glow(GlowType.IDLE)
		State.TURNING_TO_TARGET:
			_behavior_arrive.is_enabled = false
			_behavior_avoid.is_enabled = false
			_behavior_face.is_enabled = true
			_behavior_look.is_enabled = false
			_behavior_seek.is_enabled = false
			
			z_index = STATIONARY_RELATIVE_Z_INDEX
			disable_glow(GlowType.IDLE)
		State.FIRING:
			_behavior_arrive.is_enabled = false
			_behavior_avoid.is_enabled = false
			_behavior_face.is_enabled = false
			_behavior_look.is_enabled = false
			_behavior_seek.is_enabled = false
			
			z_index = STATIONARY_RELATIVE_Z_INDEX
			disable_glow(GlowType.IDLE)
			_time_elapsed_since_bullet_fired = 0.0
		_:
			push_error("Invalid state: " + new_state as String + ".")
			return
	
	_state = new_state


func _process_state(delta: float) -> void:
	match _state:
		State.IDLE:
			_process_state_idle(delta)
		State.MOVING_TO_POS:
			_process_state_moving_to_pos(delta)
		State.MOVING_TO_TARGET:
			_process_state_moving_to_target(delta)
		State.STOPPING_NEAR_TARGET:
			_process_state_stopping_near_target(delta)
		State.TURNING_TO_TARGET:
			_process_state_turning_to_target(delta)
		State.FIRING:
			_process_state_firing(delta)
		_:
			push_error("Invalid state: " + _state as String + ".")
			return


func _process_state_idle(delta: float) -> void:
	pass


func _process_state_moving_to_pos(delta: float) -> void:
	var target_pos: Vector2 = GSAIUtils.to_vector2(_arrive_target.position)
	
	if position.distance_to(target_pos) <= ARRIVE_PRECISION_PX:
		_switch_state(State.IDLE)
		return
	
	_update_avoidance_radius()


func _process_state_moving_to_target(delta: float) -> void:
	var target = _target.get_ref()
	
	var current_spot: Object = EntityUtils.get_circle_from_entity(self)
	var velocity: Vector2 = GSAIUtils.to_vector2(steering_agent.linear_velocity)
	var braking_distance: float = Utils.calculate_braking_distance( \
			velocity.length(), max_speed_px_per_sec)
	
	if target == null:
		var stop_pos: Vector2 = PositionProvider.RequestNearestStopPosition( \
				current_spot, velocity.normalized(), braking_distance)
		
		_arrive_target.position = GSAIUtils.to_vector3(stop_pos)
		_switch_state(State.MOVING_TO_POS)
		return
	
	if EntityUtils.get_distance_between_entities(self, target) \
			<= fire_range_px + braking_distance:
		var target_spot: Object = EntityUtils.get_circle_from_entity(target)
		var firing_pos: Vector2 = PositionProvider.RequestFiringPosition( \
				current_spot, target_spot, fire_range_px)
		
		_arrive_target.position = GSAIUtils.to_vector3(firing_pos)
		_switch_state(State.STOPPING_NEAR_TARGET)
		return
	
	_seek_target.position = GSAIUtils.to_vector3(target.position)
	_update_avoidance_radius()


func _process_state_stopping_near_target(delta: float) -> void:
	var stopping_pos: Vector2 = GSAIUtils.to_vector2(_arrive_target.position)
	var target = _target.get_ref()
	
	if target != null \
			and EntityUtils.get_distance_to_entity(stopping_pos, target) \
			> fire_range_px:
		_free_last_arrive_target()
		_switch_state(State.MOVING_TO_TARGET)
		return
	
	if position.distance_to(stopping_pos) <= ARRIVE_PRECISION_PX:
		if target != null:
			_switch_state(State.TURNING_TO_TARGET)
		else:
			_switch_state(State.IDLE)
		
		return
	
	_update_avoidance_radius()


func _process_state_turning_to_target(delta: float) -> void:
	var target = _target.get_ref()
	
	if target == null:
		_switch_state(State.IDLE)
		return
	
	if EntityUtils.get_distance_between_entities(self, target) > fire_range_px:
		_free_last_arrive_target()
		_switch_state(State.MOVING_TO_TARGET)
		return
	
	if EntityUtils.is_entity_facing_entity(self, target):
		_switch_state(State.FIRING)
		return
	
	_face_target.position = GSAIUtils.to_vector3(target.position)


func _process_state_firing(delta: float) -> void:
	var target = _target.get_ref()
	
	if target == null:
		_switch_state(State.IDLE)
		return
	
	if EntityUtils.get_distance_between_entities(self, target) > fire_range_px:
		_free_last_arrive_target()
		_switch_state(State.MOVING_TO_TARGET)
		return
	
	if not EntityUtils.is_entity_facing_entity(self, target):
		_switch_state(State.TURNING_TO_TARGET)
		return
	
	_fire_bullet(delta)


func _free_last_arrive_target() -> void:
	var arrive_target: Object = CSharp.Circle.new(_arrive_target.position, \
			steering_agent.bounding_radius)
	
	PositionProvider.FreePosition(arrive_target)


func _update_avoidance_radius() -> void:
	var speed: float = steering_agent.linear_velocity.length()
	
	if abs(speed - _last_avoidance_size) >= AVOIDANCE_SPEED_STEP:
		_avoidance_radius.shape.radius = speed * AVOIDANCE_AREA_SIZE_MULTIPLIER
		_last_avoidance_size = speed


func _set_target(new_target: WeakRef) -> void:
	var target = new_target.get_ref()
	
	if target != null:
		_face_target.position = GSAIUtils.to_vector3(target.position)
		_seek_target.position = GSAIUtils.to_vector3(target.position)
	
	_target = new_target


func _set_max_speed(new_max: float) -> void:
	max_speed_px_per_sec = new_max
	steering_agent.linear_speed_max = max_speed_px_per_sec
	_behavior_arrive.deceleration_radius = max_speed_px_per_sec


func _set_max_angular_speed(new_max: float) -> void:
	max_angular_speed_rad_per_sec = new_max
	steering_agent.angular_speed_max = max_angular_speed_rad_per_sec
	_behavior_look.deceleration_radius = max_angular_speed_rad_per_sec / 5
	_behavior_face.deceleration_radius = max_angular_speed_rad_per_sec / 5


func _possible_collider_sighted(object: Node2D) -> void:
	if object is Unit and object.side != side or object is Mothership \
			or object is Asteroid:
		_avoid_context.agents.append(object.steering_agent)


func _possible_collider_out_of_sight(object: Node2D) -> void:
	if object is Mothership or object is SpaceObject:
		_avoid_context.agents.erase(object.steering_agent)


func _object_sighted(object: Node2D) -> void:
	if side == Unit.Side.ENEMIES and (object is Mothership or object is Unit \
			and object.side == Unit.Side.FRIENDS):
		if _target.get_ref() == null:
			attack(object)
		else:
			for target in _target_queue:
				if target.get_ref() == object:
					return
			
			_target_queue.append(weakref(object))


func _check_target_hp_change(new_hp: int) -> void:
	if new_hp == 0:
		var targets: Array = []
		
		var i: int = 0
		while i < _target_queue.size() and _target_queue.size() != 0:
			var target: Node2D = _target_queue[i].get_ref()
			
			if target == null or target == _target.get_ref():
				_target_queue.remove(i)
				continue
			
			targets.append(target)
			i += 1
		
		if not targets.empty():
			# removing new target from the queue would require another iteration
			# over the whole array, so we will remove it the next time we need
			# to choose a new target instead to save processing time
			targets = CSharp.CSUtils.SortUnitsByDistanceToUnit(targets, self)
			attack(targets[0])


func _cleanup() -> void:
	_free_last_arrive_target()
