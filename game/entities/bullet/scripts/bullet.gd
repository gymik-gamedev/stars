class_name Bullet
extends Area2D
# Projectile with travel speed, direction, and range
# Note: You have to set the bullet's direction and side with appropriate
#       functions before it can be used.

var damage: int = 10
var range_px: float = 0.0 setget set_range
var speed_px_per_sec: float = 300.0
var direction: Vector2 = Vector2.ZERO setget set_direction

var _distance_travelled_px: float = 0.0

var _blue_to_red: Shader = \
		EntityUtils.get_shared_resource_by_name("blue_to_red.shader")

onready var _sprite: Sprite = get_node("Sprite")

func _ready() -> void:
	self.connect("body_entered", self, "_deal_damage")


func _physics_process(delta: float) -> void:
	if _distance_travelled_px < range_px:
		position += direction * speed_px_per_sec * delta
		_distance_travelled_px += speed_px_per_sec * delta
	else:
		queue_free()


func set_direction(new_direction: Vector2) -> void:
	direction = new_direction.normalized()
	rotation = Vector2.UP.angle_to(direction)


func set_range(new_range: float) -> void:
	range_px = new_range


func set_side(side: int) -> void:
	# XXX: Do not run this before _ready, it depends on onready variables!
	if side == Unit.Side.FRIENDS:
		collision_mask = 0b11010
		_sprite.material = null
	elif side == Unit.Side.ENEMIES:
		var material: ShaderMaterial = ShaderMaterial.new()
		material.shader = _blue_to_red
		
		collision_mask = 0b00001
		_sprite.material = material
	else:
		push_error("Bad type: side must be member of EntityUtils.Side enum.")


func _deal_damage(body: Node2D) -> void:
	if body is Mothership or body is SpaceObject:
		body.take_damage(damage)
		queue_free()
