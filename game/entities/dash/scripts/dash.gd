class_name Dash
extends BasicUnit
# Unit type with high speed and lower attack

func _init() -> void:
	self.max_speed_px_per_sec = 400.0
	self.max_angular_speed_rad_per_sec = PI
	max_hp = 10
	hp = max_hp
	
	firerate_bullet_per_sec = 3.0
	fire_range_px = 75.0
	_bullet_scene = EntityUtils.get_entity_scene_by_name("StrongBullet")
	
