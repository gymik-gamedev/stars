class_name Miner
extends BasicUnit
# Unit type specialized in asteroid mining

func _init() -> void:
	self.max_speed_px_per_sec = 200.0
	self.max_angular_speed_rad_per_sec = 0.5 * PI
	
	firerate_bullet_per_sec = 5.0
	fire_range_px = 75.0
	_bullet_scene = EntityUtils.get_entity_scene_by_name("WeakBullet")


func attack(target: Node2D) -> void:
	if target is Asteroid and not target is SpaceJunk:
		.attack(target)
	elif not target is Mothership and not target is SpaceObject:
		push_error("Bad value: target must be valid Mothership or SpaceObject.")
