class_name Mothership
extends StaticBody2D
# The mothership entity type
# Note: The following variables should NOT be assigned from the outside:
#       is_shield_active
#       is_shield_enabled
#       hp
#       shield_hp
#       steering_agent
#
# Use appropriate methods or create new ones instead.

signal hp_changed(health)
signal max_hp_changed(max_health)
signal shield_hp_changed(health)
signal max_shield_hp_changed(max_health)
signal shield_activated
signal shield_deactivated
signal shield_enabled
signal shield_disabled

const SHIELD_RADIUS_INCREASE_PX: float = 50.0

var max_hp: int = 1000 setget _set_max_hp
var max_shield_hp: int = 300 setget _set_max_shield_hp
var shield_regen_hps: int = 50
var shield_cooldown_sec: float = 2.0

var is_shield_active: bool = false
var is_shield_enabled: bool = true
var hp: int = max_hp setget _set_hp
var shield_hp: int = max_shield_hp setget _set_shield_hp
var steering_agent: GSAISteeringAgent = GSAISteeringAgent.new()

onready var _shield_regen_timer: Timer = get_node("ShieldRegenTimer")
onready var _animation_player: AnimationPlayer = \
		get_node("ShieldAnimationPlayer")
onready var _collision_shape: CollisionShape2D = get_node("CollisionShape2D")

func _ready() -> void:
	var radius: float = _collision_shape.shape.radius
	steering_agent.position = GSAIUtils.to_vector3(position)
	steering_agent.bounding_radius = radius * max(scale.x, scale.y)
	
	_shield_regen_timer.connect("timeout", self, "_regenerate_shield")
	
	var shape: Object = EntityUtils.get_circle_from_entity(self)
	PositionProvider.RequestPositionNearPoint(shape, position)


func take_damage(dmg: int) -> void:
	if is_shield_active:
		self.shield_hp -= dmg
	else:
		self.hp -= dmg


func activate_shield() -> void:
	if not is_shield_enabled or is_shield_active:
		return
	
	_shield_regen_timer.stop()
	is_shield_active = true
	_animation_player.play("ActivateShield")
	_collision_shape.shape.radius += SHIELD_RADIUS_INCREASE_PX
	steering_agent.bounding_radius += SHIELD_RADIUS_INCREASE_PX \
			* max(scale.x, scale.y)
	emit_signal("shield_activated")


func deactivate_shield() -> void:
	if not is_shield_active:
		return
	
	yield(disable_shield(), "completed")
	enable_shield()


func enable_shield() -> void:
	if is_shield_enabled:
		return
	
	is_shield_enabled = true
	emit_signal("shield_enabled")


func disable_shield() -> void:
	if not is_shield_enabled:
		return
	
	if is_shield_active:
		is_shield_active = false
		_animation_player.play_backwards("ActivateShield")
		_collision_shape.shape.radius -= SHIELD_RADIUS_INCREASE_PX
		steering_agent.bounding_radius -= SHIELD_RADIUS_INCREASE_PX \
				* max(scale.x, scale.y)
		emit_signal("shield_deactivated")
	
	is_shield_enabled = false
	emit_signal("shield_disabled")
	
	yield(get_tree().create_timer(shield_cooldown_sec), "timeout")
	_shield_regen_timer.start(1.0)


func _set_max_hp(new_max_hp: int) -> void:
	if new_max_hp <= 0:
		return
	
	max_hp = new_max_hp
	emit_signal("max_hp_changed", max_hp)


func _set_max_shield_hp(new_max_shield_hp: int) -> void:
	if new_max_shield_hp <= 0:
		return
	
	max_shield_hp = new_max_shield_hp
	emit_signal("max_shield_hp_changed", max_shield_hp)


func _set_hp(new_hp: int) -> void:
	if new_hp == hp or new_hp > max_hp and hp == max_hp:
		return
	
	if new_hp <= 0:
		hp = 0
		queue_free()
	elif new_hp >= max_hp:
		hp = max_hp
	else:
		hp = new_hp
	
	emit_signal("hp_changed", hp)


func _set_shield_hp(new_shield_hp: int) -> void:
	if new_shield_hp == shield_hp or new_shield_hp > max_shield_hp \
			and shield_hp == max_shield_hp:
		return
	
	if new_shield_hp <= 0:
		self.hp += new_shield_hp
		shield_hp = 0
		deactivate_shield()
	elif new_shield_hp > max_shield_hp:
		shield_hp = max_shield_hp
	else:
		shield_hp = new_shield_hp
	
	emit_signal("shield_hp_changed", shield_hp)


func _regenerate_shield() -> void:
	self.shield_hp += shield_regen_hps
