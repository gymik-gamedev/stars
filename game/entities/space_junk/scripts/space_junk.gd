class_name SpaceJunk
extends Asteroid
# Small and fast asteroid based space object

func _init() -> void:
	speed_px_per_sec = 40.0
	rotation_rad_per_sec = 0.4
	max_hp = 400
	hp = max_hp


func _set_random_scale() -> void:
	pass
