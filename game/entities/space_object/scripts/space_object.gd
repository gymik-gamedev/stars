class_name SpaceObject
extends KinematicBody2D
# Basic moveable space object entity
# Note: The following variables should NOT be assigned from the outside:
#       hp
#       steering_agent
#
# Use appropriate methods or create new ones instead.

signal hp_changed(health)
signal max_hp_changed(max_health)

var max_hp: int = 1000 setget _set_max_hp

var hp: int = max_hp setget _set_hp
var steering_agent: GSAISteeringAgent = GSAIKinematicBody2DAgent.new(self)

func _notification(what: int) -> void:
	if what == NOTIFICATION_PREDELETE:
		_cleanup()


func take_damage(dmg: int) -> void:
	self.hp -= dmg


func _set_max_hp(new_max_hp: int) -> void:
	if new_max_hp <= 0:
		return
	
	if hp > new_max_hp:
		hp = new_max_hp
	
	max_hp = new_max_hp
	emit_signal("max_hp_changed", max_hp)


func _set_hp(new_hp: int) -> void:
	if new_hp == hp or new_hp > max_hp and hp == max_hp:
		return
	
	if new_hp <= 0:
		hp = 0
		queue_free()
	elif new_hp >= max_hp:
		hp = max_hp
	else:
		hp = new_hp
	
	emit_signal("hp_changed", hp)


func _cleanup() -> void:
	pass
