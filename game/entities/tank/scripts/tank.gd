class_name Tank
extends BasicUnit
# Unit type with low speed and high attack

func _init() -> void:
	self.max_speed_px_per_sec = 100.0
	self.max_angular_speed_rad_per_sec = 0.25 * PI
	max_hp = 100
	hp = max_hp
	
	firerate_bullet_per_sec = 1.25
	fire_range_px = 300.0
	_bullet_scene = EntityUtils.get_entity_scene_by_name("WeakBullet")
