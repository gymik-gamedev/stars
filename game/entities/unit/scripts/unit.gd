class_name Unit
extends SpaceObject
# Dummy class to avoid cyclic references in BasicUnit

enum Side { INVALID, FRIENDS, ENEMIES }
enum GlowType { SELECTED, IDLE }
