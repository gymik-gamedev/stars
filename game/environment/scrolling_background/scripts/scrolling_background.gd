class_name ScrollingBackground
extends ParallaxBackground
# Scrolling background with stars

func _process(delta: float) -> void:
	scroll_offset.y += 100 * delta
