class_name Game
extends Node2D
# Root of the whole in-game world

const SPAWN_AROUND_MOTHERSHIP_RANGE_PX: float = 250.0
const DIFFICULTY_INCREASE_PERCENT: float = 0.0001
const DIFFICULTY_INCREASE_CHANCE_PERCENT: float = 0.1
const ASTEROID_SPAWN_CHANCE_PERCENT: float = 0.03
const SPACE_JUNK_SPAWN_CHANCE_PERCENT: float = 0.001
const SPACE_OBJECT_BAR_MARGIN_PX: float = 7.0
const SPACE_OBJECT_BAR_HEIGHT_PX: float = 5.0
const SPACE_OBJECT_BAR_WIDTH_RADIUS_PERCENT: float = 2.78

# This should be assign-once variable, but GDScript doesn't have that,
# so we gotta use this. Do not modify this variable after the object
# is initialized!
var BASE_RESOLUTION: Vector2 = Vector2( \
		ProjectSettings.get_setting("display/window/size/width"), \
		ProjectSettings.get_setting("display/window/size/height"))

var around_mothership_spawn_queue: Array = []
var outside_viewport_spawn_queue: Array = []
var selected_units: Array = []
var is_asteroid_spawned: bool = false
var is_space_junk_spawned: bool = false
var is_intro_enabled: bool = true setget _set_is_intro_enabled
var enemy_unit_spawn_chance_percent: float = 0.1
var resource_manager: ResourceManager = ResourceManager.new()

var game_scene: PackedScene = load("res://game/game.tscn")
var main_menu_scene: PackedScene = UIUtils.get_ui_scene_by_name("MainMenu")

onready var mothership: Mothership = get_node("Mothership")
onready var healthbar: Healthbar = get_node("HUD/Healthbar")
onready var shieldbar: Shieldbar = get_node("HUD/Shieldbar")
onready var basic_unit_button: UnitButton = get_node("HUD/BasicUnitButton")
onready var dash_unit_button: UnitButton = get_node("HUD/DashUnitButton")
onready var tank_unit_button: UnitButton = get_node("HUD/TankUnitButton")
onready var miner_unit_button: UnitButton = get_node("HUD/MinerButton")
onready var shield_button: UnitButton = get_node("HUD/ShieldButton")
onready var object_selection: ObjectSelection = get_node("ObjectSelection")
onready var target_selection: TargetSelection = get_node("TargetSelection")
onready var every_sec_timer: Timer = get_node("EverySecondTimer")
onready var materials_counter: Counter = get_node("HUD/MaterialsCounter")
onready var parts_counter: Counter = get_node("HUD/PartsCounter")
onready var pause_dialog: PauseDialog = get_node("HUD/PauseDialog")
onready var game_over_dialog: GameOverDialog = get_node("HUD/GameOverDialog")
onready var intro_dialog: TextDialog = get_node("HUD/IntroDialog")
onready var ending_dialog: TextDialog = get_node("HUD/EndingDialog")
onready var game_finished_dialog: GameOverDialog = \
		get_node("HUD/GameFinishedDialog")

func _ready():
	randomize()
	healthbar.set_tracked_object(mothership, false)
	shieldbar.set_tracked_object(mothership, false)
	basic_unit_button.connect("pressed", self, "_spawn_basic_unit")
	dash_unit_button.connect("pressed", self, "_spawn_dash_unit")
	tank_unit_button.connect("pressed", self, "_spawn_tank_unit")
	miner_unit_button.connect("pressed", self, "_spawn_miner_unit")
	shield_button.connect("pressed", self, "_toggle_mothership_shield")
	mothership.connect("hp_changed", self, "_check_hp_change", [mothership])
	mothership.connect("shield_disabled", self, "_disable_shield_button")
	mothership.connect("shield_enabled", self, "_enable_shield_button")
	object_selection.connect("objects_selected", self, "_update_selection")
	target_selection.connect("objects_selected", self, "_selection_attack")
	target_selection.connect("location_selected", self, "_selection_move")
	every_sec_timer.connect("timeout", self, "_update_environment")
	materials_counter.count = resource_manager.materials
	parts_counter.count = resource_manager.parts
	resource_manager.connect("item_out_of_stock", self, "_disable_purchasing")
	resource_manager.connect("item_in_stock", self, "_enable_purchasing")
	resource_manager.connect("materials_count_changed", materials_counter, \
			"set_count")
	resource_manager.connect("parts_count_changed", parts_counter, "set_count")
	pause_dialog.connect("restart_requested", self, "_restart_game")
	pause_dialog.connect("quit_to_menu_requested", self, "_return_to_menu")
	game_over_dialog.connect("restart_requested", self, "_restart_game")
	game_over_dialog.connect("quit_to_menu_requested", self, "_return_to_menu")
	game_finished_dialog.connect("restart_requested", self, "_restart_game")
	game_finished_dialog.connect("quit_to_menu_requested", self, \
			"_return_to_menu")
	
	if is_intro_enabled:
		intro_dialog.show()


func _physics_process(delta: float) -> void:
	for item in around_mothership_spawn_queue:
		var unit: BasicUnit = BasicUnit.get_unit_by_name(item["unit_type"])
		
		if unit != null:
			_spawn_around_mothership(item["mothership"], unit, \
					item["unit_side"], item["max_distance_px"])
		
		around_mothership_spawn_queue.erase(item)
	
	for item in outside_viewport_spawn_queue:
		var unit: BasicUnit = BasicUnit.get_unit_by_name(item["unit_type"])
		
		if unit != null:
			_spawn_outside_viewport(unit, item["unit_side"], \
					item["distance_px"])
		
		outside_viewport_spawn_queue.erase(item)


func queue_spawn_around_mothership(mothership: Mothership, unit_type: String, \
		unit_side: int, max_distance_px: float) -> void:
	around_mothership_spawn_queue.append({"mothership": mothership, \
			"unit_type": unit_type, "unit_side": unit_side, \
			"max_distance_px": max_distance_px})


func queue_spawn_outside_viewport(unit_type: String, \
		unit_side: int, distance_px: float) -> void:
	outside_viewport_spawn_queue.append({"unit_type": unit_type, \
			"unit_side": unit_side, "distance_px": distance_px})


func _spawn_around_mothership(mothership: Mothership, unit: BasicUnit, \
		unit_side: int, max_distance_px: float) -> void:
	# TODO: can get stuck in infinite loop if there is no more space to place
	#		units
	# TODO: add margin from mothership (and its shield)
	var query: Physics2DShapeQueryParameters = \
			Physics2DShapeQueryParameters.new()
	var space_state: Physics2DDirectSpaceState = \
			get_world_2d().direct_space_state
	
	var unit_shape: Shape2D = \
			unit.get_node("CollisionShape2D").shape
	var mothership_shape: CircleShape2D = \
			mothership.get_node("CollisionShape2D").shape
	
	query.set_shape(unit_shape)
	var radius_min_px: float = mothership_shape.radius
	var radius_max_px: float = max_distance_px
	
	while true:
		var radius_px: float = rand_range(radius_min_px, radius_max_px)
		var angle_rad: float = rand_range(0.0, 2 * PI)
		
		var relative_position: Vector2 = Vector2(radius_px * sin(angle_rad), \
				-radius_px * cos(angle_rad))
		var absolute_position: Vector2 = mothership.position + relative_position
		
		var transform: Transform2D = Transform2D(angle_rad, absolute_position)
		query.transform = transform
		
		if space_state.intersect_shape(query, 1).empty():
			unit.transform = transform
			add_child(unit)
			unit.set_side(unit_side)
			unit.connect("hp_changed", self, "_check_hp_change", [unit])
			_add_healthbar_to_space_object(unit)
			break


func _spawn_outside_viewport(unit: BasicUnit, unit_side:int, \
		distance_px: float) -> void:
	unit.position = Utils.rand_point_on_viewport_edge( \
			BASE_RESOLUTION, distance_px)
	add_child(unit)
	unit.set_side(unit_side)
	unit.move_to_pos(mothership.position)
	# add mothership to the target queue so that units attack it when they run
	# out of targets, even if they haven't detected it yet
	unit._target_queue = [weakref(mothership)]
	unit.connect("hp_changed", self, "_check_hp_change", [unit])
	_add_healthbar_to_space_object(unit)


func _spawn_basic_unit() -> void:
	if resource_manager.buy_item(ResourceManager.Item.BASIC_UNIT):
		queue_spawn_around_mothership(mothership, "BasicUnit", \
				Unit.Side.FRIENDS, SPAWN_AROUND_MOTHERSHIP_RANGE_PX)


func _spawn_dash_unit() -> void:
	if resource_manager.buy_item(ResourceManager.Item.DASH):
		queue_spawn_around_mothership(mothership, "Dash", \
				Unit.Side.FRIENDS, SPAWN_AROUND_MOTHERSHIP_RANGE_PX)


func _spawn_tank_unit() -> void:
	if resource_manager.buy_item(ResourceManager.Item.TANK):
		queue_spawn_around_mothership(mothership, "Tank", \
				Unit.Side.FRIENDS, SPAWN_AROUND_MOTHERSHIP_RANGE_PX)


func _spawn_miner_unit() -> void:
	if resource_manager.buy_item(ResourceManager.Item.MINER):
		queue_spawn_around_mothership(mothership, "Miner", \
				Unit.Side.FRIENDS, SPAWN_AROUND_MOTHERSHIP_RANGE_PX)


func _update_environment() -> void:
	if enemy_unit_spawn_chance_percent < 1:
		enemy_unit_spawn_chance_percent += DIFFICULTY_INCREASE_PERCENT
	
	if Utils.rand_chance(enemy_unit_spawn_chance_percent):
		var unit: String = Utils.rand_choice([ "BasicUnit", "Dash", "Tank" ])
		queue_spawn_outside_viewport(unit, Unit.Side.ENEMIES, 100.0)
	
	if not is_asteroid_spawned and not is_space_junk_spawned \
			and Utils.rand_chance(ASTEROID_SPAWN_CHANCE_PERCENT):
		_spawn_asteroid()
	
	if not is_space_junk_spawned and not is_asteroid_spawned \
			and Utils.rand_chance(SPACE_JUNK_SPAWN_CHANCE_PERCENT):
		_spawn_space_junk()
	
	if mothership.is_shield_active:
		resource_manager.buy_item(ResourceManager.Item.SHIELD_ONE_SEC)


func _spawn_asteroid() -> void:
	var asteroid: Asteroid = \
			EntityUtils.get_entity_scene_by_name("Asteroid").instance()
	var path_duration_sec: float = \
			BASE_RESOLUTION.length() / asteroid.speed_px_per_sec
	
	add_child(asteroid)
	asteroid.connect("hp_changed", self, "_check_hp_change", [asteroid])
	_add_healthbar_to_space_object(asteroid)
	
	is_asteroid_spawned = true
	yield(get_tree().create_timer(path_duration_sec), "timeout")
	is_asteroid_spawned = false


func _spawn_space_junk() -> void:
	var space_junk: SpaceJunk = \
			EntityUtils.get_entity_scene_by_name("SpaceJunk").instance()
	var path_duration_sec: float = \
			BASE_RESOLUTION.length() / space_junk.speed_px_per_sec
	
	add_child(space_junk)
	space_junk.connect("hp_changed", self, "_check_hp_change", [space_junk])
	_add_healthbar_to_space_object(space_junk)
	
	is_space_junk_spawned = true
	yield(get_tree().create_timer(path_duration_sec), "timeout")
	is_space_junk_spawned = false


func _add_healthbar_to_space_object(object: SpaceObject) -> void:
	# XXX: Do not run before object's _ready!
	var obj_radius: float = object.steering_agent.bounding_radius
	
	var bar_margin: float = SPACE_OBJECT_BAR_MARGIN_PX
	var bar_width: float = SPACE_OBJECT_BAR_WIDTH_RADIUS_PERCENT * obj_radius
	var bar_size: Vector2 = Vector2(bar_width, SPACE_OBJECT_BAR_HEIGHT_PX)
	
	EntityUtils.attach_healthbar_to_object(object, bar_size, bar_margin)


func _toggle_mothership_shield() -> void:
	if not mothership.is_shield_active:
		if resource_manager.buy_item(ResourceManager.Item.SHIELD_ONE_SEC):
			mothership.activate_shield()
	else:
		mothership.deactivate_shield()


func _disable_shield_button() -> void:
	shield_button.disabled = true


func _enable_shield_button() -> void:
	shield_button.disabled = false


func _update_selection(selection: Array) -> void:
	for unit_ref in selected_units:
		var unit: Unit = unit_ref.get_ref()
		
		if unit == null:
			continue
		
		unit.disable_glow(Unit.GlowType.SELECTED)
	
	selected_units.clear()
	
	for object in selection:
		if object is BasicUnit and object.side == Unit.Side.FRIENDS:
			object.enable_glow(Unit.GlowType.SELECTED)
			selected_units.append(weakref(object))


func _selection_attack(targets: Array) -> void:
	if not selected_units.empty():
		if targets.size() == 1 and targets[0] is Unit \
				and targets[0].side == Unit.Side.FRIENDS:
			_selection_move(targets[0].position)
			return
		
		var i: int = 0
		while i < selected_units.size() and targets.size() != 0:
			var unit: Unit = selected_units[i].get_ref()
			
			if unit == null:
				selected_units.remove(i)
				continue
			
			var target = targets[i % targets.size()]
			
			if not target is SpaceObject or target is Unit \
					and target.side != Unit.Side.ENEMIES:
				targets.remove(i % targets.size())
				continue
			
			unit.attack(target)
			i += 1


func _selection_move(location: Vector2) -> void:
	if not selected_units.empty():
		var units: Array = []
		
		for unit_ref in selected_units:
			var unit: Unit = unit_ref.get_ref()
			
			if unit == null:
				continue
			
			units.append(unit)
		
		units = CSharp.CSUtils.SortUnitsByTimeToReachPoint(units, location)
		
		for unit in units:
			unit.move_to_pos(location)


func _check_hp_change(new_health: int, object: Node2D) -> void:
	if new_health == 0:
		var reward: int = -1
		
		if object is SpaceJunk:
			reward = ResourceManager.Reward.SPACE_JUNK_MINED
		elif object is Asteroid:
			reward = ResourceManager.Reward.ASTEROID_MINED
		elif object is Unit and object.side == Unit.Side.ENEMIES:
			reward = ResourceManager.Reward.ENEMY_UNIT_DESTROYED
		elif object is Mothership:
			game_over_dialog.show()
			return
		else:
			return
		
		resource_manager.redeem_reward(reward)


func _disable_purchasing(item: int) -> void:
	match item:
		ResourceManager.Item.BASIC_UNIT:
			basic_unit_button.disabled = true
		ResourceManager.Item.DASH:
			dash_unit_button.disabled = true
		ResourceManager.Item.TANK:
			tank_unit_button.disabled = true
		ResourceManager.Item.MINER:
			miner_unit_button.disabled = true
		ResourceManager.Item.SHIELD_ONE_SEC:
			mothership.disable_shield()
		_:
			push_error("Unknown item: " + item as String + ".")


func _enable_purchasing(item: int) -> void:
	match item:
		ResourceManager.Item.BASIC_UNIT:
			basic_unit_button.disabled = false
		ResourceManager.Item.DASH:
			dash_unit_button.disabled = false
		ResourceManager.Item.TANK:
			tank_unit_button.disabled = false
		ResourceManager.Item.MINER:
			miner_unit_button.disabled = false
		ResourceManager.Item.SHIELD_ONE_SEC:
			mothership.enable_shield()
		ResourceManager.Item.WARP_DRIVE_REPAIR:
			_end_game()
		_:
			push_error("Unknown item: " + item as String + ".")


func _restart_game() -> void:
	var new_game = game_scene.instance()
	new_game.is_intro_enabled = false
	get_parent().add_child(new_game)
	queue_free()


func _return_to_menu() -> void:
	get_parent().add_child(main_menu_scene.instance())
	queue_free()


func _end_game() -> void:
	ending_dialog.show()
	ending_dialog.connect("hidden", game_finished_dialog, "show")


func _set_is_intro_enabled(new: bool) -> void:
	if intro_dialog != null and not new:
		intro_dialog.hide()
	
	is_intro_enabled = new
