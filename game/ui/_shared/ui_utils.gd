class_name UIUtils
extends Object
# Various utilities for working with UI nodes

const UI_PATH: String = Utils.BASE_PATH + "/ui"

static func get_ui_scene_by_name(ui_name: String) -> PackedScene:
	var scene: PackedScene = Utils.get_scene_by_name(ui_name, UI_PATH)
	
	if scene == null:
		push_error("UI node not found: " + ui_name + ".")
	
	return scene


static func get_shared_resource_by_name(resource_filename: String) -> Resource:
	return Utils.get_resource_by_name(resource_filename, \
			UI_PATH + "/" + Utils.SHARED_DIR_NAME)
