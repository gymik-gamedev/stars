class_name Counter
extends Label
# Visual representation of a count
# Note: The following variables should NOT be changed after _ready:
#       digits
#       font_size

export var digits: int = 6
export var count: int = 0 setget set_count
export var font_size: int = 16

func _ready():
	var font: DynamicFont = DynamicFont.new()
	font.font_data = UIUtils.get_shared_resource_by_name("falstin_regular.ttf")
	font.size = font_size
	
	add_font_override("font", font)
	_update_counter()


func set_count(new_count: int) -> void:
	count = new_count
	_update_counter()


func _update_counter() -> void:
	text = ("%0" + digits as String + "d") % count
