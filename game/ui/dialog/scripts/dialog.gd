class_name Dialog
extends Control
# Base class for interactive dialogs

signal shown
signal hidden
signal restart_requested
signal quit_to_menu_requested

func _ready() -> void:
	if visible:
		show()


func show() -> void:
	visible = true
	get_tree().paused = true
	emit_signal("shown")


func hide() -> void:
	visible = false
	get_tree().paused = false
	emit_signal("hidden")


func _restart_game() -> void:
	emit_signal("restart_requested")
	hide()


func _quit_to_menu() -> void:
	emit_signal("quit_to_menu_requested")
	hide()
