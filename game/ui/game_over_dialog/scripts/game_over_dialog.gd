class_name GameOverDialog
extends Dialog
# Interactive dialog shown when the player dies

onready var restart_button: Button = get_node("Splash/Content/RestartButton")
onready var quit_button: Button = get_node("Splash/Content/QuitButton")

func _ready() -> void:
	restart_button.connect("pressed", self, "_restart_game")
	quit_button.connect("pressed", self, "_quit_to_menu")
