class_name Healthbar
extends ProgressBar
# Bar showing health of mothership and space objects

# These should be constants, but GDScript doesn't allow changing constants
# in derived classes nor assign-once variables, so we gotta use this.
# Do not change these values after the object is initialized!
var VALUE_CHANGED_SIGNAL_NAME: String = "hp_changed"
var RANGE_CHANGED_SIGNAL_NAME: String = "max_hp_changed"

var _tracked_object: WeakRef = weakref(null)
var _is_attached: bool = false
var _relative_pos: Vector2 = Vector2.ZERO

func _physics_process(delta: float) -> void:
	var tracked_object = _tracked_object.get_ref()
	
	if tracked_object == null:
		return
	
	if _is_attached:
		rect_global_position = tracked_object.position + _relative_pos
		
		# rect_rotation uses degrees instead of radians for some reason
		rect_rotation = -rad2deg(tracked_object.rotation)


func set_tracked_object(object: Node2D, is_attached: bool) -> void:
	if object is Mothership or object is SpaceObject:
		var tracked_object = _tracked_object.get_ref()
		
		if tracked_object != null:
			tracked_object.disconnect(VALUE_CHANGED_SIGNAL_NAME, self, \
					"_update_healthbar_value")
			tracked_object.disconnect(RANGE_CHANGED_SIGNAL_NAME, self, \
					"_update_healthbar_range")
		
		_tracked_object = weakref(object)
		_relative_pos = rect_global_position - object.position
		_is_attached = is_attached
		max_value = object.max_hp
		value = object.hp
		
		object.connect(VALUE_CHANGED_SIGNAL_NAME, self, \
				"_update_healthbar_value")
		object.connect(RANGE_CHANGED_SIGNAL_NAME, self, \
				"_update_healthbar_range")
	else:
		push_error("Bad type: object must be Mothership or SpaceObject.")


func _update_healthbar_value(new_value: int) -> void:
	value = new_value


func _update_healthbar_range(new_max_value: int) -> void:
	max_value = new_max_value
