class_name MainMenu
extends Control
# Main menu of the game

var game_scene: PackedScene = preload("res://game/game.tscn")

onready var start_button: Button = get_node("ButtonGroup/StartButton")
onready var copying_info_button: Button = \
		get_node("ButtonGroup/CopyingInfoButton")
onready var quit_button: Button = get_node("ButtonGroup/QuitButton")
onready var copying_page1: TextDialog = get_node("CopyingDialog/Page1")
onready var copying_page2: TextDialog = get_node("CopyingDialog/Page2")

func _ready() -> void:
	start_button.connect("pressed", self, "_start_game")
	copying_info_button.connect("pressed", copying_page1, "show")
	copying_page1.connect("hidden", copying_page2, "show")
	quit_button.connect("pressed", self, "_quit_game")


func _start_game() -> void:
	get_parent().add_child(game_scene.instance())
	queue_free()


func _quit_game() -> void:
	get_tree().quit()
