class_name ObjectSelection
extends Node2D
# Allows drag selection of in-game objects

signal objects_selected(objects)

var ACTIVATION_BUTTON: int = BUTTON_LEFT
var SELECTION_COLOR: Color = Color.aqua

export var is_enabled = true

var _is_dragging: bool = false
var _drag_origin: Vector2 = Vector2.ZERO
var _finalized_selection_area: Rect2 = Utils.RECT2_EMPTY

func _physics_process(delta: float) -> void:
	if not is_enabled:
		return
	
	if _finalized_selection_area != Utils.RECT2_EMPTY:
		var selection_rect: RectangleShape2D = RectangleShape2D.new()
		selection_rect.extents = _finalized_selection_area.size / 2
		
		var origin: Vector2 = _finalized_selection_area.position
		var end: Vector2 = origin + _finalized_selection_area.size
		
		var query: Physics2DShapeQueryParameters = \
				Physics2DShapeQueryParameters.new()
		var space_state: Physics2DDirectSpaceState = \
				get_world_2d().direct_space_state
		
		query.set_shape(selection_rect)
		query.transform = Transform2D(0.0, (origin + end) / 2)
		
		var matched_colliders: Array = space_state.intersect_shape(query)
		var selected_objects: Array = []
		
		for item in matched_colliders:
			selected_objects.append(item.collider)
		
		_process_selected_objects(selected_objects)
		_finalized_selection_area = Utils.RECT2_EMPTY


func _draw() -> void:
	if not is_enabled:
		return
	
	if _is_dragging:
		_draw_selection(_drag_origin, get_global_mouse_position())


func _input(event: InputEvent) -> void:
	# This part is not in _unhandled_input to prevent clashing with control
	# nodes and their handling of hover.
	if not is_enabled:
		return
	
	if event is InputEventMouseMotion:
		update()


func _unhandled_input(event: InputEvent) -> void:
	if not is_enabled:
		return
	
	if event is InputEventMouseButton \
			and event.button_index == ACTIVATION_BUTTON:
		if event.is_pressed():
			_is_dragging = true
			_drag_origin = event.position
		elif _is_dragging:
			_is_dragging = false
			update()
			
			var drag_end: Vector2 = event.position
			_queue_selection_query(_drag_origin, drag_end)


func _draw_selection(origin: Vector2, end: Vector2) -> void:
	var selection_rect: Rect2 = Rect2(origin, end - origin)
	var outer_color: Color = SELECTION_COLOR
	var inner_color: Color = Color(outer_color.r, outer_color.g, \
			outer_color.b, 0.4)
	
	draw_rect(selection_rect, outer_color, false)
	draw_rect(selection_rect, inner_color, true)


func _queue_selection_query(origin: Vector2, end: Vector2) -> void:
	_finalized_selection_area = Rect2(origin, end - origin)


func _process_selected_objects(objects: Array) -> void:
	emit_signal("objects_selected", objects)
