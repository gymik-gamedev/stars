class_name PauseDialog
extends Dialog
# Interactive dialog shown when the game is paused

onready var resume_button: Button = get_node("Splash/Content/ResumeButton")
onready var restart_button: Button = get_node("Splash/Content/RestartButton")
onready var quit_button: Button = get_node("Splash/Content/QuitButton")

func _ready() -> void:
	resume_button.connect("pressed", self, "hide")
	restart_button.connect("pressed", self, "_restart_game")
	quit_button.connect("pressed", self, "_quit_to_menu")


func _unhandled_input(event: InputEvent) -> void:
	if InputMap.event_is_action(event, "pause") and not event.is_pressed():
		if not visible and not get_tree().paused:
			show()
		elif visible:
			hide()
