class_name Shieldbar
extends Healthbar
# Bar showing shield health of mothership

const SHIELDBAR_DISABLED_MODULATION: Color = Color.webgray

func _init() -> void:
	VALUE_CHANGED_SIGNAL_NAME = "shield_hp_changed"
	RANGE_CHANGED_SIGNAL_NAME = "max_shield_hp_changed"


func set_tracked_object(object: Node2D, is_attached: bool) -> void:
	if object is Mothership:
		var tracked_object = _tracked_object.get_ref()
		
		if tracked_object != null:
			tracked_object.disconnect("shield_activated", self, \
					"_enable_shieldbar")
			tracked_object.disconnect("shield_deactivated", self, \
					"_disable_shieldbar")
		
		.set_tracked_object(object, is_attached)
		max_value = object.max_shield_hp
		value = object.shield_hp
		
		if not object.is_shield_active:
			modulate = SHIELDBAR_DISABLED_MODULATION
		
		object.connect("shield_activated", self, "_enable_shieldbar")
		object.connect("shield_deactivated", self, "_disable_shieldbar")
	else:
		push_error("Bad type: object must be Mothership.")


func _enable_shieldbar() -> void:
	# White modulation = no modulation
	modulate = Color.white


func _disable_shieldbar() -> void:
	modulate = SHIELDBAR_DISABLED_MODULATION
