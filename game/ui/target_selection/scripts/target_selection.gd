class_name TargetSelection
extends ObjectSelection
# Allows drag selection of in-game objects and click selection of location

signal location_selected(location)

const LOCATION_SELECTION_MAX_SIZE: Vector2 = Vector2(30.0, 30.0)

func _init() -> void:
	ACTIVATION_BUTTON = BUTTON_RIGHT
	SELECTION_COLOR = Color.red


func _process_selected_objects(objects: Array) -> void:
	var area: Rect2 = _finalized_selection_area
	if objects.empty() and area.size < LOCATION_SELECTION_MAX_SIZE:
		var selection_mid: Vector2 = area.position + area.size / 2
		emit_signal("location_selected", selection_mid)
	else:
		emit_signal("objects_selected", objects)
