class_name TextDialog
extends Dialog
# Simple text prompt for storytelling, tutorials, etc.

onready var continue_button: Button = get_node("Splash/Content/ContinueButton")

func _ready() -> void:
	continue_button.connect("pressed", self, "hide")
